#include <iostream>
#include <WS2tcpip.h>
#include <MSWSock.h>
#include <vector>
#include <thread>
#include <mutex>
#include "protocol.h"
#pragma comment(lib, "WS2_32.lib")
#pragma comment(lib, "mswsock.lib")
using namespace std;



constexpr auto MAX_PACKET_SIZE = 255;
constexpr auto MAX_BUF_SIZE = 1024;
constexpr auto MAX_USER = 10;
enum ENUMOP{ OP_RECV, OP_SEND, OP_ACCEPT };
enum C_STATUS{ ST_FREE, ST_ALLOC, ST_ACTIVE}; // 클라이언트의 상태를 나타내주기위함
//위 3가지 상태로 나눈이유는 m_connected가 false이지만 접속이 완료된 상태의 클라이언트에
// 다른 쓰레드가 위의 경우의 id를 할당할 경우에대비하기위함.
struct EXOVER
{
	WSAOVERLAPPED	Over;
	ENUMOP			Op; //이 구조체가 리시브인지 샌드인지 엑셉트인지.
	char			IO_Buf[MAX_BUF_SIZE];
	union
	{
		WSABUF			WSABuf;
		SOCKET			c_socket;
	};
};
struct CLIENT
{
	mutex m_cl;
	SOCKET m_Sock; //datarace
	int m_ID; 
	EXOVER m_RecvOver;
	int m_PrevSize;
	char m_PacketBuf[MAX_PACKET_SIZE];
	C_STATUS m_Status;
	//bool m_Connected; //접속한 여부파악. datarace

	//게임콘텐츠정보
	short x, y;
	char m_name[MAX_ID_LEN + 1]; // datarace
};

CLIENT g_Clients[MAX_USER];
HANDLE g_iocp;
SOCKET ListenSocket;

void Process_Packet(int user_id, char* buf);
void send_login_ok_packet(int user_id);
void send_packet(int user_id, void* p);
void disconnect(int user_id);
void recv_packet_construct(int user_id, int iobytes);

void send_packet(int user_id, void* p) //그떄그때 패킷이 다르다.
{// 이 함수를 호출할때 lock으로 보호를 받고있다.
	char* buf = reinterpret_cast<char*>(p);
	CLIENT& u = g_Clients[user_id];

	EXOVER* exover = new EXOVER;
	exover->Op = OP_SEND;
	ZeroMemory(&exover->Over, sizeof(exover->Over));
	exover->WSABuf.buf = exover->IO_Buf;
	exover->WSABuf.len = buf[0];
	memcpy(exover->IO_Buf, buf, buf[0]);

	WSASend(u.m_Sock, &exover->WSABuf, 1, NULL, 0, &exover->Over, NULL);
}

void send_login_ok_packet(int user_id)
{
	sc_packet_login_ok p;
	p.exp = 0;
	p.hp = 0;
	p.id = user_id;
	p.level = 0;
	p.size = sizeof(p);
	p.type = S2C_LOGIN_OK;
	p.x = g_Clients[user_id].x;
	p.y = g_Clients[user_id].y;

	send_packet(user_id, &p);
}
void send_move_packet(int user_id, int mover)
{
	sc_packet_move p;
	
	p.id = mover;
	p.size = sizeof(p);
	p.type = S2C_MOVE;//S2C_LOGIN_OK;
	p.x = g_Clients[mover].x;
	p.y = g_Clients[mover].y;

	send_packet(user_id, &p);
}
void do_move(int user_id, int direction)
{
	CLIENT& u = g_Clients[user_id];
	int x = u.x;
	int y = u.y;

	switch (direction)
	{
	case D_UP: if (y > 0) y--; break;
	case D_DOWN: if (y < WORLD_HEIGHT - 1) y++; break;
	case D_LEFT: if (x > 0) x--; break;
	case D_RIGHT:if (x < WORLD_WIDTH - 1) x++; break;
	default:
		cout << "Unknown direction from client move packet";
		DebugBreak();
		exit(-1);
	}
	u.x = x;
	u.y = y;
	for (auto& client : g_Clients)
	{
		client.m_cl.lock();
		if (ST_ACTIVE == client.m_Status)
			send_move_packet(client.m_ID, user_id); //client에게 나의 정보 cliemt.m_ID -> user_id
		client.m_cl.unlock();
	}
}
void send_enter_packet(int user_id, int o_id)
{
	sc_packet_enter p;
	p.id = o_id;
	p.size = sizeof(p);
	p.type = S2C_ENTER;
	p.x = g_Clients[o_id].x;
	p.y = g_Clients[o_id].y;
	strcpy_s(p.name, g_Clients[o_id].m_name);
	p.o_type = O_PLAYER;
	send_packet(user_id, &p);
}
void enter_game(int user_id, char name[])
{
	//원래 락을 해줘야하지만  덮어써도 문제가없다 근데 이건 상당히위험함 나중에할것임.
	g_Clients[user_id].m_cl.lock();

	strcpy_s(g_Clients[user_id].m_name, name);
	g_Clients[user_id].m_name[MAX_ID_LEN] = NULL;
	send_login_ok_packet(user_id);

	
	for (int i = 0; i < MAX_USER; i++)
	{
		if (user_id == i) continue;   
		g_Clients[i].m_cl.lock(); //위와 같이 이중 락을 걸어 데드락이댄다. ㄱ래서 위에 콘티뉴코드를추가함.
		if (g_Clients[i].m_Status == ST_ACTIVE)
		{
			if (user_id != i) //나한태는 보낼필요없다. 내자신의정보를
			{
				send_enter_packet(user_id, i); //나에게 보낸다. 다른놈의정보를
				send_enter_packet(i, user_id); //나의정보를 다른놈에게 
			}
		}
		g_Clients[i].m_cl.unlock();
	}

	g_Clients[user_id].m_Status = ST_ACTIVE;
	g_Clients[user_id].m_cl.unlock(); //여기다 언락을하면 너무 크리티컬섹션 범위가 크다.
}

void Process_Packet(int user_id, char* buf)
{
	switch (buf[1])
	{
	case C2S_LOGIN:
	{
		cs_packet_login* packet = reinterpret_cast<cs_packet_login*>(buf);
		
		enter_game(user_id, packet->name); //게임에들어왔을떄처리
	}
		break;
	case C2S_MOVE:
	{
		cs_packet_move* packet = reinterpret_cast<cs_packet_move*>(buf);
		do_move(user_id, packet->direction);
	}
		break;
	default:
		cout << "Unknown Packet Type Error";
		DebugBreak();
		exit(-1);
	}
}

void worker_thread()
{
	while (true)
	{


		DWORD iobytes;
		ULONG_PTR key;
		WSAOVERLAPPED* over;
		GetQueuedCompletionStatus(g_iocp, &iobytes, &key, &over, INFINITE);

		EXOVER* exover = reinterpret_cast<EXOVER*>(over);
		int user_id = static_cast<int>(key);
		CLIENT& cl = g_Clients[user_id];

		
		switch (exover->Op)
		{
		case OP_RECV:
		{
			if ((0 == iobytes))
			{
				disconnect(user_id);
			}
			else
			{
				recv_packet_construct(user_id, iobytes); //패킷조립

				ZeroMemory(&cl.m_RecvOver.Over, sizeof(cl.m_RecvOver.Over));
				DWORD flags = 0;

				WSARecv(cl.m_Sock, &cl.m_RecvOver.WSABuf, 1, NULL, &flags, &cl.m_RecvOver.Over, NULL);
			}
		}
		break;
		case OP_SEND:
		{
			if ((0 == iobytes))
			{
				disconnect(user_id);
			}
			delete exover;
		}
		break;
		case OP_ACCEPT:
		{
			//여러쓰레드에서 동시에 접속할수도있기때문에 뮤텍스를 걸어줘야한다 여기도.
			int user_id = -1;
			for (int i = 0; i < MAX_USER; ++i)
			{
				lock_guard<mutex> gl{ g_Clients[i].m_cl };  // c++의 기능 루프를빠져나갈때마다 자동으로 unlock
				if (ST_FREE == g_Clients[i].m_Status)
				{
					g_Clients[i].m_Status = ST_ALLOC;
					user_id = i;

					
					break;   
				}
			}

			SOCKET c_socket = exover->c_socket;
			if (-1 == user_id)      //send_login_fail_packet();
			{
				closesocket(c_socket);   //동접이 꽉찬경우임 여기들어오면
			}
			else
			{

				//iocp에 등록하기 위 create 함수랑 다른것.
				
				CreateIoCompletionPort(reinterpret_cast<HANDLE>(c_socket), g_iocp, user_id, 0);



				CLIENT& newClient = g_Clients[user_id];

				newClient.m_PrevSize = 0;
				newClient.m_RecvOver.Op = OP_RECV;
				ZeroMemory(&newClient.m_RecvOver.Over, sizeof(newClient.m_RecvOver.Over));
				newClient.m_RecvOver.WSABuf.buf = newClient.m_RecvOver.IO_Buf;
				newClient.m_RecvOver.WSABuf.len = MAX_BUF_SIZE;
				newClient.m_Sock = c_socket;
				newClient.x = rand() % WORLD_WIDTH;
				newClient.y = rand() % WORLD_HEIGHT;
				DWORD flags = 0;


				//우리는 iocp를쓸거기떄문에 null
				WSARecv(newClient.m_Sock, &newClient.m_RecvOver.WSABuf, 1, NULL, &flags, &newClient.m_RecvOver.Over, NULL);
			}

			//accpet를 새로 받는과정임 밑에는
			c_socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
			exover->c_socket = c_socket;
			ZeroMemory(&exover->Over, sizeof(exover->Over));
			AcceptEx(ListenSocket, c_socket, exover->IO_Buf, NULL, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &exover->Over); //소켓을 만들어서 리턴하지않음, 미리 소켓을 만들어놓고 이 만들어논 소켓을 클라이언트와연결해줌.
		}
		break;
		}
	}
}
void initialize_clients()
{
	//여기는 락을걸필요가없다. 싱글쓰레드에서 돌고있는부분이기떄문에
	for (int i = 0; i < MAX_USER; ++i)
	{
		g_Clients[i].m_ID = i;
		g_Clients[i].m_Status = ST_FREE;
	}
}
void send_leave_packet(int user_id, int o_id)
{
	sc_packet_enter p;
	p.id = o_id;
	p.size = sizeof(p);
	p.type = S2C_LEAVE;

	send_packet(user_id, &p);
}
void disconnect(int user_id)
{
	g_Clients[user_id].m_cl.lock();
	g_Clients[user_id].m_Status = ST_ALLOC;
	send_leave_packet(user_id, user_id);
	closesocket(g_Clients[user_id].m_Sock);
	for (auto& client : g_Clients)
	{
		if (client.m_ID == user_id) continue;
		client.m_cl.lock();
		if (ST_ACTIVE == client.m_Status)
			send_leave_packet(client.m_ID, user_id); //접속종료했으니 다 지워라 
		client.m_cl.unlock();
	}
	g_Clients[user_id].m_Status = ST_FREE;
	g_Clients[user_id].m_cl.unlock();
}
void recv_packet_construct(int user_id, int iobytes)
{
	CLIENT& cu = g_Clients[user_id];
	EXOVER& r_o = cu.m_RecvOver;
	
	int rest_bytes = iobytes;
	char* p = r_o.IO_Buf;
	int packet_size = 0;
	if (0 != cu.m_PrevSize) packet_size = cu.m_PacketBuf[0];
	while (rest_bytes > 0) // 아직처리할 데이터가 남아있으면.
	{
		if (0 == packet_size)
			packet_size = *p;
		if (packet_size <= rest_bytes + cu.m_PrevSize) //지난번에 받은거랑 지금받은거랑 합쳣을때 패킷사이즈보다 크면 완성가능
		{
			memcpy(cu.m_PacketBuf + cu.m_PrevSize, p, packet_size - cu.m_PrevSize);
			p += packet_size - cu.m_PrevSize;
			rest_bytes -= packet_size - cu.m_PrevSize;
			packet_size = 0;
			Process_Packet(user_id, cu.m_PacketBuf);
			cu.m_PrevSize = 0;
		}
		else //완성못하면 저장을해야함.
		{
			memcpy(cu.m_PacketBuf + cu.m_PrevSize, p, rest_bytes);
			cu.m_PrevSize += rest_bytes; // 추가해줌 이전에받았던 데이터사이즈를
			rest_bytes = 0;
			p += rest_bytes;
			
		}
	}
	
}
int main()
{
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 2), &WSAData);
	
	ListenSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);

	SOCKADDR_IN ServerAddr;
	memset(&ServerAddr, 0x00, sizeof(SOCKADDR_IN));
	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_port = htons(SERVER_PORT);
	ServerAddr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);

	::bind(ListenSocket, reinterpret_cast<sockaddr*>(&ServerAddr), sizeof(ServerAddr));

	listen(ListenSocket, SOMAXCONN);

	

	
	//iocp 만들기.
	g_iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);
	
	initialize_clients();
	
	//엑셉트도 iocp로 받자.
	CreateIoCompletionPort(reinterpret_cast<HANDLE>(ListenSocket), g_iocp, 999, 0);

	SOCKET c_socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	EXOVER accept_over;
	ZeroMemory(&accept_over.Over, sizeof(accept_over.Over));
	accept_over.Op = OP_ACCEPT; //ACCPET에서는 WSABUF 를쓰지않는다.
	accept_over.c_socket = c_socket;
	AcceptEx(ListenSocket, c_socket, accept_over.IO_Buf, NULL, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &accept_over.Over); //소켓을 만들어서 리턴하지않음, 미리 소켓을 만들어놓고 이 만들어논 소켓을 클라이언트와연결해줌.

	vector<thread> worker_threads;
	for (int i = 0; i < 4; ++i) // 4는 4코어이므로 4로한것임.
		worker_threads.emplace_back(worker_thread);

	for (auto& th : worker_threads)
		th.join();
	return 0;
}